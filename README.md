# Seminar on iterative methods for linear systems of equations
[![Binderhub](https://img.shields.io/badge/Binderhub-Jupyterlab-orange)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Flehrenfeld%2Fws2223-prosem-iterative/HEAD?labpath=overview.ipynb)
[![Binderhub](https://img.shields.io/badge/Binderhub-JupyterClassic-orange)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Flehrenfeld%2Fws2223-prosem-iterative/HEAD?filepath=overview.ipynb)
[![lite-badge](https://img.shields.io/badge/Jupyterlite-yellow)](https://lehrenfeld.pages.gwdg.de/ws2223-prosem-iterative)


